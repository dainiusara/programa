﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace programa2
{
    public partial class Form1 : Form
    {
        public int speed_left = 4;  // kamuoliuko greitis
        public int speed_top = 4;
        public int point = 0;     // tasku kiekis
        public Form1()
        {
            InitializeComponent();

            timer1.Enabled = true;
            Cursor.Hide(); // Paslepti pelyte

            this.FormBorderStyle = FormBorderStyle.None; // paslepti borderius
            this.TopMost = true; //forma bus priekyje
            this.Bounds = Screen.PrimaryScreen.Bounds; //per visa ekrana 

            racket.Top = playground.Bottom - (playground.Bottom / 10); //raketes pozicija

            gameover_lbl.Left = (playground.Width / 2) - (gameover_lbl.Width / 2); // teksto pozicija i centra
            gameover_lbl.Top = (playground.Height / 2) - (gameover_lbl.Height / 2);
            gameover_lbl.Visible = false; //paslepti lentele

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            racket.Left = Cursor.Position.X - (racket.Width);   // nustato raketes viduri pagal pelyte
            ball.Left += speed_left;   //judinti kamuoliuka
            ball.Top += speed_top;
            if (ball.Bottom >= racket.Top && ball.Bottom <= racket.Bottom && ball.Left >= racket.Left && ball.Right <= racket.Right) // raketes susidurimas
            {
                Random r = new Random();
                speed_top += 2;
                speed_left = (r.Next(0, 10));
                //speed_left += 2;
                speed_top = -speed_top; //keisti trajektorija
                //speed_top = -speed_top; //keisti trajektorija
                point += 1;
                points_lbl.Text = point.ToString();

                ball.Height = (r.Next(100, 500));
                racket.Width = (r.Next(100, 500));
                ball.BackColor = Color.FromArgb(r.Next(0, 149), r.Next(0, 149), r.Next(0, 149)); // skirtingas kamuoliukas
                playground.BackColor = Color.FromArgb(r.Next(150, 255), r.Next(150, 255), r.Next(150, 255)); // skirtingas background

            }
            if (ball.Left <= playground.Left)
            {
                speed_left = -speed_left;
            }
            if (ball.Right >= playground.Right)
            {
                speed_left = -speed_left;
            }
            if (ball.Top <= playground.Top)
            {
                speed_top = -speed_top;
            }
            if (ball.Bottom >= playground.Bottom)
            {
                timer1.Enabled = false;   // kamuoliukas iskrito zaidimo pabaiga
                gameover_lbl.Visible = true;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { this.Close(); } //spausti Escape, kad iseiti
            if (timer1.Enabled == false)
            if (e.KeyCode == Keys.F1)
            {
                ball.Top = 50;
                ball.Left = 50;
                speed_left = 4;
                speed_top = 4;
                point = 0;
                points_lbl.Text = "";
                timer1.Enabled = true;
                gameover_lbl.Visible = false;
                ball.BackColor = Color.Red;
                playground.BackColor = Color.Green;
            }
        }

        private void gameover_lbl_Click(object sender, EventArgs e)
        {

        }
    }
}
